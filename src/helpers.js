import handleRecipe from './cmd/recipe/recipe'
import handleDessert from './cmd/recipe/dessert'
import { handleMainCourse, replyFromMainCourse } from './cmd/recipe/mainCourse'
import { handleStarter, replyFromStarter } from './cmd/recipe/starter'
import { replyFromApero, handleApero } from './cmd/apero'

function buildReplyMarkup(data) {
  const keys = Object.keys(data)
  const keyboard = []
  for (let key of keys) {
    const entry = {
      text: data[key].title,
      callback_data: key
    }
    keyboard.push(entry)
  }
  return JSON.stringify({ inline_keyboard: [keyboard] })
}

function getRandomValue(data, key) {
  const values = data[key].values
  const id = Math.floor(Math.random() * values.length)
  return values[id]
}

function getFirstReplyFromCallback(callback) {
  switch (callback) {
    case 'starter':
      return handleStarter
    case 'mainCourse':
      return handleMainCourse
    case 'dessert':
      return handleDessert
    case 'cook':
      return handleRecipe
    case 'apero':
      return handleApero
    default:
      return false
  }
}

function getSecondReplyFromCallback(callback) {
  switch (callback) {
    case 'cocktail':
    case 'snack':
    case 'otherApero':
      return replyFromApero

    case 'light':
    case 'common':
    case 'asia':
    case 'otherStarter':
      return replyFromStarter

    case 'healthy':
    case 'fish':
    case 'meat':
    case 'pasta':
      return replyFromMainCourse

    default:
      return false
  }
}

const buildUrl = param =>
  `https://www.marmiton.org/recettes/index/categorie/${param}?rcp=0`

export {
  buildUrl,
  buildReplyMarkup,
  getRandomValue,
  getFirstReplyFromCallback,
  getSecondReplyFromCallback
}
