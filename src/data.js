const apero = {
  cocktail: {
    title: 'Cocktail 🍹',
    values: [
      'cocktail-aperitif',
      'mojito',
      'margarita',
      'punch',
      'sangria',
      'cocktail-champagne',
      'cocktail-rhum',
      'cocktail-vodka',
      'cocktail-gin',
      'cocktail-whisky',
      'pina-colada',
      'tequila-sunrise',
      'caipirinha',
      'spritz',
      'bloody-mary'
    ]
  },
  snack: {
    title: 'Amuse Bouche 🥜',
    values: ['bouchee-ou-amuse-bouche']
  },
  otherApero: {
    title: 'Autres 🤦‍♂️',
    values: ['apero-leger', 'apero-pas-cher']
  }
}

const starter = {
  light: {
    title: 'Healthy 🥗',
    values: [
      'entree-legere',
      'tomate-mozzarella',
      'autres-crudites',
      'carottes-rapees',
      'gaspacho-soupfroide',
      'tartare',
      'carpaccio'
    ]
  },
  common: {
    title: 'Cake / Charcuterie',
    values: ['saucisson', 'terrine-pate', 'foie-gras', 'cake-sale', 'flan-sale']
  },
  asia: {
    title: 'Asiatique',
    values: [
      'poisson-cru',
      'raviolis',
      'ravioles',
      'dim-sum',
      'gyoza',
      'nems',
      'rouleaux-de-printemps'
    ]
  },
  otherStarter: {
    title: 'Autres 🤦‍♂️',
    values: ['entree-facile', 'entree-rapide']
  }
}

const mainCourse = {
  healthy: {
    title: 'Healthy 🥗',
    values: [
      'dal',
      'tofu',
      'œufs',
      'aubergine',
      'vegetarian',
      'plat-vegetarien',
      'curry-de-legumes',
      'galette-de-legumes',
      'galette-de-cereales',
      'hamburger-vegetarien',
      'jardiniere-ou-poelee-de-legumes'
    ]
  },
  fish: {
    title: 'Poisson 🐟',
    values: [
      'poisson',
      'poisson-pane',
      'fruits-de-mer',
      'poisson-facile',
      'poisson-au-four',
      'saumon-en-sauce',
      'poisson-en-sauce',
      'soupe-de-poisson',
      'filets-de-poisson',
      'poisson-en-croute',
      'poisson-en-papillote'
    ]
  },
  meat: {
    title: 'Viande 🥩',
    values: ['viande', 'viande-rotie', 'viande-en-sauce', 'boulettes-de-viande']
  },
  pasta: {
    title: 'Pasta / Riz 🍚',
    values: [
      'quinoa',
      'risotto',
      'polenta',
      'lasagne',
      'gnocchis',
      'macaronis',
      'spaghetti',
      'tagliatelle',
      'plat-de-riz',
      'autres-pates',
      'pates-en-sauce',
      'gratin-de-pates',
      'plats-au-fromage',
      'pates-riz-semoule',
      'lasagne-vegetarienne'
    ]
  }
}

export { apero, starter, mainCourse }
