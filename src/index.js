import TelegramBot from 'node-telegram-bot-api'

import handleStart from './cmd/start'
import { handleApero } from './cmd/apero'
import handleRecipe from './cmd/recipe/recipe'
import {
  getFirstReplyFromCallback,
  getSecondReplyFromCallback
} from './helpers'

const token = '1118284700:AAGusrSz4nAj5iS2UyreVX7vS45S6cGqNLA'

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, { polling: true })

bot.onText(/start/, msg => handleStart(bot, msg))
bot.onText(/apero/, msg => handleApero(bot, msg))
bot.onText(/recette/, msg => handleRecipe(bot, msg))

bot.on('callback_query', ctx => {
  const { message, data: callbackData } = ctx
  console.log('## callbackData:', callbackData) // eslint-disable-line
  let replyFn = getFirstReplyFromCallback(callbackData)
  if (replyFn) {
    console.log('## replyFn: 1', replyFn) // eslint-disable-line
    return replyFn(bot, message)
  }

  replyFn = getSecondReplyFromCallback(callbackData)

  if (replyFn) {
    console.log('## replyFn: 2', replyFn) // eslint-disable-line
    return replyFn(bot, ctx)
  }
})

bot.on('polling_error', console.log) // eslint-disable-line
