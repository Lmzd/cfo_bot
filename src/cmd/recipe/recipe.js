function handleRecipe(bot, msg) {
  const chat_id = msg.chat.id

  const text = `Alors comme çà tu sais cuisiner !!! Bonne blague 👌
Allez choisi ce que tu veux faire ... `

  const ops = {
    reply_markup: JSON.stringify({
      // Added JSON.stringify()
      inline_keyboard: [
        [
          {
            text: 'Entrée 🥑',
            callback_data: 'starter'
          },
          {
            text: 'Plat 🥩',
            callback_data: 'mainCourse'
          },
          {
            text: 'Dessert 🧁',
            callback_data: 'dessert'
          }
        ]
      ]
    })
  }
  bot.sendMessage(chat_id, text, ops)
}

export default handleRecipe
