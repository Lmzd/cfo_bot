import { starter } from '../../data'
import { buildReplyMarkup, buildUrl, getRandomValue } from '../../helpers'

function replyFromStarter(bot, ctx) {
  const message = ctx.message
  const chatId = message.chat.id

  const value = getRandomValue(starter, ctx.data)

  bot.sendMessage(
    chatId,
    `Régale toi bien ${message.chat.first_name} !!! ${buildUrl(value)}`
  )
}

function handleStarter(bot, msg) {
  const chatId = msg.chat.id
  const reply_markup = buildReplyMarkup(starter)
  const ops = { reply_markup }
  bot.sendMessage(
    chatId,
    "Une entrée donc, j'en ai plein à te proposer 🙂 comme une salade de chez Papa par exemple.",
    ops
  )
}

export { handleStarter, replyFromStarter }
