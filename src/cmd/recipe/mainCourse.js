import { mainCourse } from '../../data'
import { buildReplyMarkup, buildUrl, getRandomValue } from '../../helpers'

function replyFromMainCourse(bot, ctx) {
  const message = ctx.message
  const chatId = message.chat.id
  console.log('## message:', message) // eslint-disable-line
  const value = getRandomValue(mainCourse, ctx.data)
  bot.sendMessage(
    chatId,
    `Régale toi bien ${message.chat.first_name} !!! ${buildUrl(value)}`
  )
}

function handleMainCourse(bot, msg) {
  const chatId = msg.chat.id
  const reply_markup = buildReplyMarkup(mainCourse)
  const ops = { reply_markup }
  bot.sendMessage(
    chatId,
    "Dis moi tout, j'ai plein de recette vraiment bonne à te proposer 🙂 comme les célèbres pâtes gratinné du Lorada par exemple.",
    ops
  )
}

export { handleMainCourse, replyFromMainCourse }
