import { apero } from '../data'
import { buildReplyMarkup, buildUrl, getRandomValue } from '../helpers'

function replyFromApero(bot, ctx) {
  const message = ctx.message
  const chatId = message.chat.id
  const value = getRandomValue(apero, ctx.data)
  bot.sendMessage(chatId, `Santé !!! 🍻 ${buildUrl(value)}`)
}

function handleApero(bot, msg) {
  const chatId = msg.chat.id

  const reply_markup = buildReplyMarkup(apero)
  const ops = { reply_markup }

  const hour = new Date().getHours()
  console.log('## msg:', msg) // eslint-disable-line
  let text
  if ((hour > 5 && hour < 11) || (hour > 13 && hour < 17)) {
    text = `Écoute moi ${msg.from.first_name}, je sais que tu as soif et saches que même ton CFO serait le premier à te suivre pour un apéro.
Mais là il faut se résonner quand même, il n'est que ${hour} heures ...
Bon, si tu inscites`
  } else {
    text = `OYÉ OYÉ !! ${msg.from.first_name} a soif et a raison d'avoir soif !! Que veux-tu boire pour l'apéro ?`
  }
  bot.sendMessage(chatId, text, ops)
}

export { handleApero, replyFromApero }
