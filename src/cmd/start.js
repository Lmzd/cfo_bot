async function handleStart(bot, msg) {
  const chat_id = msg.chat.id

  const text = `Bienvenu ${msg.from.first_name} !!!
Je suis ton CFO et je suis l'assistant parfait pour passer un bon confinement.
Que puis je faire pour toi ? `

  const ops = {
    reply_markup: JSON.stringify({
      // Added JSON.stringify()
      inline_keyboard: [
        [
          {
            text: 'Cuisiner 🍽',
            callback_data: 'cook'
          },
          {
            text: 'Apéro 🍻',
            callback_data: 'apero'
          }
        ]
      ]
    })
  }

  bot.sendMessage(chat_id, text, ops)
}

export default handleStart
