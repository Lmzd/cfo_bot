#!/bin/sh

set -x
set -e

BOT_NAME="cfo_bot"
BOT_IMAGE_NAME="cfo_telegram_bot"

cd $BOT_NAME
pwd

echo "UPGRADE"

git fetch -p
git pull origin master

echo "BUILD"

docker build -t $BOT_IMAGE_NAME .

echo "DEPLOY"

docker stop $BOT_NAME
docker rm $BOT_NAME
docker run -d --name $BOT_NAME -p 8081:8081 $BOT_IMAGE_NAME

echo "SUCCESS"

